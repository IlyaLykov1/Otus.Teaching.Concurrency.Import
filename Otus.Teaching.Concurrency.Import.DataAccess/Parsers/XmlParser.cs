﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _fileName;

        public XmlParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {
            List<Customer> customers = new List<Customer>();

            using (var reader = new StreamReader(_fileName))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<Customer>),
                    new XmlRootAttribute("Customers"));
                customers = (List<Customer>)deserializer.Deserialize(reader);
            }

            return customers;
        }
    }
}