﻿namespace Otus.Teaching.Concurrency.Import.DataAccess.Data
{
    public static class DbInitializer
    {
        public static void Initialize(AppDbContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
