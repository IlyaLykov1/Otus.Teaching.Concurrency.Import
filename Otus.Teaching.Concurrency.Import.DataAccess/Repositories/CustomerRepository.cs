using Otus.Teaching.Concurrency.Import.DataAccess.Data;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private AppDbContext _dbContext;
        public CustomerRepository()
        {
            _dbContext = new AppDbContext();
        }
        public void AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}