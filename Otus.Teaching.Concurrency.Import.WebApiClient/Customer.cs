﻿using System.Text.Json.Serialization;

namespace Otus.Teaching.Concurrency.Import.WebApiClient
{
    public class Customer
    {
        [JsonPropertyName("fullName")]
        public string FullName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }
    }
}
