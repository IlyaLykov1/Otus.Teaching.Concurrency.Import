﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.WebApiClient;
using System.Net;
using System.Text;
using System.Text.Json;

namespace WebAPIClient
{
    class Program
    {
        private static readonly HttpClient _client = new HttpClient();

        static async Task Main(string[] args)
        {
            await FindCustomerById();
            await AddCustomer();
            await AddRandomCustomer();
            await ShowAllCustomers();
        }

        private static async Task ShowAllCustomers()
        {
            Console.WriteLine("All customers: ");
            await ProcessCustomers();
        }

        private static async Task AddRandomCustomer()
        {
            Console.WriteLine("Adding random customer: ");
            var randomCustomer = GenerateCustomer();
            var requestRandom = await AddCustomer(randomCustomer);
            if (requestRandom.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine($"New random customer has been added");
            }
            else
            {
                Console.WriteLine("Bad request");
            }
        }

        private static async Task AddCustomer()
        {
            Console.WriteLine("Enter full name: ");
            var fullName = Console.ReadLine();
            Console.WriteLine("Enter Email: ");
            var email = Console.ReadLine();
            Console.WriteLine("Enter phone: ");
            var phone = Console.ReadLine();

            var request = await AddCustomer(fullName, email, phone);
            if (request.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine($"New customer has been added");
            }
            else
            {
                Console.WriteLine("Bad request");
            }
        }

        private static async Task FindCustomerById()
        {
            Console.WriteLine("Enter customer Id:");
            if (Int32.TryParse(Console.ReadLine(), out int id))
            {
                var responce = await GetCustomerResponce(id);
                if (responce.StatusCode == HttpStatusCode.NotFound)
                {
                    Console.WriteLine($"Customer with Id: {id} not found");
                }
                else
                {
                    var customer = await GetCustomer(responce);
                    Console.WriteLine($"Id: {id}, Name: {customer.FullName}, Email: {customer.Email}, Phone: {customer.Phone}");
                }
            }
        }

        private static Customer GenerateCustomer()
        {
            var customer =  RandomCustomerGenerator.GenerateCustomer();
            return new Customer() { FullName = customer.FullName, Email = customer.Email, Phone = customer.Phone };
        }

        private static async Task<HttpResponseMessage> AddCustomer(string fullName, string email, string phone)
        {
            var customer = new Customer() { FullName = fullName, Email = email, Phone = phone };
            var json = JsonSerializer.Serialize<Customer>(customer);
            var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await _client.PostAsync($"https://localhost:44374/api/customer", content);
            return request;
        }
        private static async Task<HttpResponseMessage> AddCustomer(Customer customer)
        {
            var json = JsonSerializer.Serialize<Customer>(customer);
            var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await _client.PostAsync($"https://localhost:44374/api/customer", content);
            return request;
        }

        private static async Task<Customer> GetCustomer(HttpResponseMessage responce)
        {
            var json = await responce.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<Customer>(await responce.Content.ReadAsStringAsync());
        }

        private static async Task<HttpResponseMessage> GetCustomerResponce(int id)
        {
            return await _client.GetAsync($"https://localhost:44374/api/customer/{id}");
        }

        private static async Task ProcessCustomers()
        {
            var streamTask = _client.GetStreamAsync("https://localhost:44374/api/customer");
            var customers = await JsonSerializer.DeserializeAsync<List<Customer>>(await streamTask);

            foreach (var repo in customers)
            {
                Console.WriteLine(repo.FullName);
            }
        }
    }
}
