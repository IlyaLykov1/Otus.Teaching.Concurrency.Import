﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Api.Models;

namespace Otus.Teaching.Concurrency.Import.Api
{
    public class ApiAppDbContext : DbContext
    {
        public ApiAppDbContext(DbContextOptions<ApiAppDbContext> options) : base(options)
        {

        }
        public DbSet<Customer> Customers { get; set; }
    }
}
