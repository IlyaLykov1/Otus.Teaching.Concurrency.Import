﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataLoader : IDataLoader
    {
        private readonly List<Customer> _customers;
        private CustomerRepository[] _repositories;
        private List<List<Customer>> _subCollections;
        private readonly int _threadCount = 8;

        public DataLoader(List<Customer> customers)
        {
            _customers = customers;
            PopulateRepositories();
            PopulateSubCollections();
        }

        private void PopulateRepositories()
        {
            _repositories = new CustomerRepository[_threadCount];
            for (int i = 0; i < _repositories.Length; i++)
            {
                _repositories[i] = new CustomerRepository();
            }
        }

        private void PopulateSubCollections()
        {
            _subCollections = new List<List<Customer>>(_threadCount);
            for (int i = 0; i < _threadCount; i++)
            {
                _subCollections.Add(new List<Customer>());
            }
            for (int i = 0; i < _customers.Count; i++)
            {
                _subCollections[i % _threadCount].Add(_customers[i]);
            }
        }

        public void LoadData()
        {
            ManualResetEvent[] manualEvents = new ManualResetEvent[_threadCount];

            Stopwatch sp = new Stopwatch();
            sp.Start();

            for (int i = 0; i < _threadCount; i++)
            {
                manualEvents[i] = new ManualResetEvent(false);
                var tParam = new ThreadParams(_subCollections[i], _repositories[i], manualEvents[i]);

                ThreadPool.QueueUserWorkItem(new WaitCallback(AddCustomers), tParam);
            }

            WaitHandle.WaitAll(manualEvents);

            sp.Stop();
            Console.WriteLine($"{sp.ElapsedMilliseconds} ms");
        }

        private void AddCustomers(object tParams)
        {
            var tParamsInner = (ThreadParams)tParams;

            foreach (var customer in tParamsInner.Customers)
            {
                tParamsInner.Repository.AddCustomer(customer);
            }

            tParamsInner.Repository.SaveChanges();
            tParamsInner.ManualEvent.Set();
        }

        private class ThreadParams
        {
            public List<Customer> Customers { get; set; }
            public CustomerRepository Repository { get; set; }
            public ManualResetEvent ManualEvent { get; set; }
            public ThreadParams(List<Customer> customers, CustomerRepository repository, ManualResetEvent manualEvent)
            {
                Customers = customers;
                Repository = repository;
                ManualEvent = manualEvent;
            }
        }
    }
}
